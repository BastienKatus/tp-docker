## Bastien KATUSZYNSKI
## Maxime LEFEBVRE
#

# TP1 - Docker

## Database
Création d'un dossier
``` 
mkdir tp1
cd tp1/
touch Dockerfile
```
```
docker pull postgres
```
Ecriture du dockerfile :
```
nano Dockerfile

>
FROM postgres:14.1-alpine
ENV POSTGRES_DB=db \
POSTGRES_USER=usr \
POSTGRES_PASSWORD=pwd
```
Création de l'image
```
docker build -t bastienkatus/postgres
```

Run l'image :
```
docker run -p 8888:5000 --name postgres bastienkatus/postgres
```
Initialisation de la database :
```
docker network create app-network
```
```
docker run \
-p "8090:8080" \
--net=app-network \
--name=adminer \
-d \
adminer
```
Création du schéma SQL :
```
touch 01-CreateScheme.sql
nano 01-CreateScheme.sql

CREATE TABLE public.departments
(
 id      SERIAL      PRIMARY KEY,
 name    VARCHAR(20) NOT NULL
);

CREATE TABLE public.students
(
 id              SERIAL      PRIMARY KEY,
 department_id   INT         NOT NULL REFERENCES departments (id),
 first_name      VARCHAR(20) NOT NULL,
 last_name       VARCHAR(20) NOT NULL
);
```
Insertion des données SQL :
```
touch 02-InsertData.sql
nano 02-InsertData.sql

INSERT INTO departments (name) VALUES ('IRC');
INSERT INTO departments (name) VALUES ('ETI');
INSERT INTO departments (name) VALUES ('CGP');

INSERT INTO students (department_id, first_name, last_name) VALUES (1, 'Eli', 'Copter');
INSERT INTO students (department_id, first_name, last_name) VALUES (2, 'Emma', 'Carena');
INSERT INTO students (department_id, first_name, last_name) VALUES (2, 'Jack', 'Uzzi');
INSERT INTO students (department_id, first_name, last_name) VALUES (3, 'Aude', 'Javel');
```

On modifie le Dockerfile : (On copie les scritps SQL sur l'image)
```
COPY [Scripts SQL] /docker-entrypoint-initdb.d/
```
On rebuild et on rerun :
```
docker build -t bastienkatus/postgres .

docker run --network app-network --name some-postgres -e POSTGRES_PASSWORD=pwd -d postgres-bastien
```
On se connecter à localhost:8090 et on peut accéder aux données
![Image postGre](loginpostgres.png)

Pour la persistance des données :

On créé un volume
```
mkdir volume
```
et on relance le run avec la commande
```
docker run --network app-network --name some-postgres -e POSTGRES_PASSWORD=pwd -d postgres-bastien -v volume:/var/lib/postgresql/data
```

Question 1-1 :
Documenation au dessus.

## Java

On créé un fichier à la source /tpJAVA

On ouvre le Dockerfile et on y insère :

### JDK -> <u>Compiler</u>
```
FROM openjdk:11
# Build Main.java
# TODO : in next steps (not now)

# Executer un fichier java -> Construction de l'image 

COPY Main.java .
RUN javac Main.java
```

### JRE -> <u>Exécuter</u>

On rajoute la ligne
```
CMD ["java", "Main"]
```
On rebuild et on relance

Question 1.2 :
On a un multistage build car il faut d'abbord compiler puis executer le programme. Le dockerfile comprend une première partie permettant de copier le fichier Main.java puis de le RUN. La deuxième partie permet d'executer la commande java Main.

## Backend API

Dockerfile :
```
# Build
FROM maven:3.8.6-amazoncorretto-17 AS myapp-build
ENV MYAPP_HOME /opt/myapp
WORKDIR $MYAPP_HOME
COPY /simpleapi/pom.xml .
COPY /simpleapi/src ./src
RUN mvn package -DskipTests

# Run
FROM amazoncorretto:17
ENV MYAPP_HOME /opt/myapp
WORKDIR $MYAPP_HOME
COPY --from=myapp-build $MYAPP_HOME/target/*.jar $MYAPP_HOME/myapp.jar

ENTRYPOINT java -jar myapp.jar
```

## Docker compose :

On supprime toutes les images
```
docker rm -f .....
```

On télécharge le zip présent sur le git pour récupérer la correction de la première partie du TP

On vérifie la présence des tous les fichiers, notamment du .env contenant les variables d'environnement.

```
docker-compose build
docker-compose up -d
```
Question 1-3 :
docker-compose build : Construire les services
docker-compose up : Lance tous les services
docker-compose down : Stop tous les services
docker-compose up -d : Lancer en arrière plan
docker ps : Lister les processus

Question 1-4 :
Documentation au dessus.

Question 1-5 :
Documentation et image à rajouter sur dockerhub

#

# TP2 - GitHub

Question 2-1 :
Testcontainers are java libraries that allow to run a bunch of docker containers while testing.

Création d'un fichier .github/workflows à la racine du projet et d'un fichier main.yml :
```
name: CI devops 2023
on:
  #to begin you want to launch this job in main and develop
  push:
    branches: main 
  pull_request:

jobs:
  test-backend: 
    runs-on: ubuntu-22.04
    steps:
     #checkout your github code using actions/checkout@v2.5.0
      - uses: actions/checkout@v2.5.0

     #do the same with another action (actions/setup-java@v3) that enable to setup jdk 17
      - name: Set up JDK 17
        uses: actions/setup-java@v3
        with:
            java-version: '17'
            distribution: 'zulu''

     #finally build your app with the latest command
      - name: Build and test with Maven
        run: cd ./[chemin]/ simple-api mvn clean verify -e
```

Par la suite, on push ce fichier pour que github reconnaissance le github action.
On vérifie le bon fonctionnement des jobs dans l'onglet Action

Question 2-2 :
Comme c'est un .yml on a pas osé faire de commentaires car ca aurait tout casser.
* On a donc ajouter les actions sur la branche main
* On a ajouté une action pour le setup java.
* On a ajouté une action qui lance le maven clean et verify.

